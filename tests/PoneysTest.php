<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {

	protected $poneys;

	protected function setUp(){
		$this->poneys= new Poneys();
	}

	protected function tearDown(){
		$this->poneys=null;
	}
    public function test_removePoneyFromField() {

      // Action
      $this->poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $this->poneys->getCount());
    }

	public function test_addPoneyTooField() {
 
	  $this->poneys->addPoneyTooField(1);

      // Assert
      $this->assertEquals(9, $this->poneys->getCount());
	}

	/**
	 * @expectedException Exception
	 */
	public function test_removeTooMunchPoney(){

	  $this->poneys->removePoneyFromField(9);

	}

	/**
	 * @dataProvider additionProvider
	 */
	public function test_multipleRemovePoney($nbPoney, $tot){

		$this->poneys->removePoneyFromField($nbPoney);

		$this->assertEquals($tot, $this->poneys->getCount());

	}
	public function additionProvider(){
		return array(
			array(3,5),
			array(4,4),
			array(2,6),
			array(1,7)
			);
	}
    public function test_mockGetNames(){

        $stub = $this->getMockBuilder('Poneys')
					 ->getMock();

		$stub->method('getNames')
			 ->willReturn('Albert');

		$this->assertEquals('Albert',$stub->getNames());
    }
	public function test_champPlein(){
		 $this->poneys = new Poneys();
		$this->poneys->addPoneyTooField(7);
		 $this->assertTrue($this->poneys->champPlein());
	}
	public function test_champPasPlein(){
		 $this->assertNotTrue($this->poneys->champPlein());
	}
  }
 ?>
