module.exports = function(grunt) {

    // Configuration de Grunt
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            build:{
                src:'js/*.js',
                dest: 'dist/<%= pkg.name %>.min.js'

            }

        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.registerTask('default', ['concat']);

};
